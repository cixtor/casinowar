package main

import (
	"errors"
	"fmt"
	"math/rand"
)

// cardDeckSize is the number of cards in a standard card deck.
//
// Clubs:    ♣ Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King
// Diamonds: ♦ Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King
// Hearts:   ♥ Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King
// Spades:   ♠ Ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King
const cardDeckSize = 52

// suitSpade represents the Spade suit of the French playing cards.
const suitSpade rune = '\u2660'

// suitHeart represents the Heart suit of the French playing cards.
const suitHeart rune = '\u2665'

// suitClub represents the Club suit of the French playing cards.
const suitClub rune = '\u2663'

// suitDiamond represents the Diamond suit of the French playing cards.
const suitDiamond rune = '\u2666'

type Card struct {
	// Suite is either Spade, Heart, Club or Diamond.
	Suit rune
	// Value is a number from one to thirteen.
	Value int
	// Used is True if the card has been revealed, False otherwise.
	Used bool
}

type BatchOfCards struct {
	// Cards is the list of cards in the batch.
	Cards []Card
	// DiscardPile is the list of cards that have been discarded.
	DiscardPile []Card
	// Total is the number of cards in the batch.
	Total int
	// Quota is the number of unseen cards.
	Quota int
}

func NewBatchOfCards(size int) *BatchOfCards {
	b := new(BatchOfCards)

	b.Cards = make([]Card, cardDeckSize*size)

	// Unbox 312 cards (six standard 52-card decks) into the batch.
	for i := 0; i < size; i++ {
		copy(b.Cards[cardDeckSize*i:cardDeckSize*(i+1)], cardDeck())
	}

	b.Total += cardDeckSize * size
	b.Quota += cardDeckSize * size

	return b
}

// Shuffle rearranges (a deck of cards) by sliding the cards over each other
// quickly, but only the cards that are still in the deck, facing down.
func (b *BatchOfCards) Shuffle() {
	rand.Shuffle(len(b.Cards), func(i int, j int) {
		b.Cards[i], b.Cards[j] = b.Cards[j], b.Cards[i]
	})
}

var errCannotDiscard = errors.New("cannot discard cards")

func (b *BatchOfCards) DiscardCard() error {
	if b.Quota < 1 {
		return errCannotDiscard
	}

	b.DiscardPile = append(b.DiscardPile, b.Cards[b.Quota-1])
	b.Cards = b.Cards[0 : b.Quota-1]
	b.Quota--

	return nil
}

func (b *BatchOfCards) Deal() (Card, error) {
	if b.Quota < 1 {
		return Card{}, errNoMoreCards
	}

	card := b.Cards[b.Quota-1]
	card.Used = true
	b.DiscardPile = append(b.DiscardPile, card)
	b.Cards = b.Cards[0 : b.Quota-1]
	b.Quota--

	return card, nil
}

func cardDeck() []Card {
	var i int

	cards := make([]Card, cardDeckSize)

	for _, suit := range []rune{suitSpade, suitHeart, suitClub, suitDiamond} {
		for j := 1; j <= cardDeckSize/4; j++ {
			cards[i] = Card{Suit: suit, Value: j}
			i++
		}
	}

	return cards
}

func (b BatchOfCards) String() string {
	var s string

	for i := 0; i < b.Total; i++ {
		s += fmt.Sprintf("%s ", b.Cards[i])

		if i%13 == 12 {
			s += "\n"
		}
	}

	return s
}

func (c Card) String() string {
	var v string

	switch c.Value {
	case 1:
		v = "A"
	case 10:
		v = "X"
	case 11:
		v = "J"
	case 12:
		v = "Q"
	case 13:
		v = "K"
	default:
		v = fmt.Sprintf("%d", c.Value)
	}

	// if c.Used {
	// 	return fmt.Sprintf("\x1b[0;2m%s%s\x1b[0m", v, string(c.Suit))
	// }
	if c.Suit == suitHeart || c.Suit == suitDiamond {
		return fmt.Sprintf("\x1b[0;31m%s%s\x1b[0m", v, string(c.Suit))
	} else {
		return fmt.Sprintf("%s%s", v, string(c.Suit))
	}
}
