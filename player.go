package main

import (
	"errors"
	"fmt"
)

// maximumPlayers is the highest number of simultaneous players.
//
// Maximum eight players: seven players plus one player/dealer.
const maximumPlayers int = 7

var errNotEnoughPlayers = errors.New("not enough players")

var errPlayerHasNoChips = errors.New("player has no chips")

var errPlayerHasFolded = errors.New("player has folded")

type Player struct {
	ID    uint8
	Name  string
	Chips int
	Bet   int
}

type PlayerHand struct {
	Player *Player
	Card   Card
}

func (p *Player) BetChips() error {
	if p.Chips == 0 {
		return errPlayerHasNoChips
	}

	var bet int

	fmt.Printf("%s, place your bet (up to %d chips, 0 to fold): ", p.Name, p.Chips)
	fmt.Scanf("%d", &bet)

	if bet == 0 {
		return errPlayerHasFolded
	}

	if bet > p.Chips {
		// Stop the game if the player bets more chips than they have.
		return fmt.Errorf("invalid bet from %s (%d > %d)", p.Name, bet, p.Chips)
	}

	p.Bet += bet
	p.Chips -= bet

	return nil
}

func (p *Player) WinBet() {
	p.Chips += p.Bet * 2
	p.Bet = 0
}

func (p *Player) LoseBet() {
	p.Bet = 0
}

func (p *Player) Surrender() {
	p.Chips += p.Bet / 2
	p.Bet = 0
}

var errNotEnoughChips = errors.New("not enough chips")

func (p *Player) WarWager() error {
	bet := p.Bet

	if bet > p.Chips {
		return errNotEnoughChips
	}

	p.Bet += bet
	p.Chips -= bet

	return nil
}
