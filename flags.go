package main

import (
	"strings"
)

type PlayerList []string

func (p *PlayerList) Set(value string) error {
	names := strings.Split(value, ",")
	for _, name := range names {
		if name == "" {
			continue
		}
		*p = append(*p, name)
	}
	return nil
}

func (p *PlayerList) String() string {
	return strings.Join(*p, ",")
}
