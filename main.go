package main

import (
	"flag"
	"fmt"
	"io"
	"os"
)

var players PlayerList

func main() {
	flag.Var(&players, "players", "People who will play the game (separate with commas)")

	flag.Usage = func() {
		fmt.Println("Casino War")
		fmt.Println()
		fmt.Println("For the sake of simplicity, each player will start with 1,000\nchips with the smallest value (U$1/chip). A player exits the\ngame when they cannot bet anymore. The game ends when the\nnumber of available players is lower than two.")
		fmt.Println()
		flag.PrintDefaults()
	}

	flag.Parse()

	game := NewCasinoWar()

	for _, name := range players {
		if err := game.AddPlayer(name, 1000); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	game.Shuffle()

	for {
		err := game.Play()

		if err == io.EOF {
			break
		}

		if err != nil {
			fmt.Println(err)
			break
		}
	}
}
