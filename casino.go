package main

import (
	"errors"
	"fmt"
	"io"
	"math/rand"
	"time"
)

var errNoMoreCards = errors.New("the card deck is empty")

var errNotEnoughCards = errors.New("not enough cards to deal")

// CasinoWar is one of the easier table games to learn, Casino War uses a
// standard 52-card deck and requires no skill to play. The game lets players
// bet on who will draw a higher card – the player or the dealer. Here’s how it
// works.
//
// THE OBJECT OF THE GAME
//
// The object of Casino War is simple: players make a bet on whether their card
// will beat the dealer’s card. The player and dealer are each dealt a single
// card. Highest card wins. If both cards are equal, the player can choose
// "War", and the player and the dealer will each get an additional card to
// compare.
type CasinoWar struct {
	// seats is the number of players in the table.
	seats int
	// players is the list of people in the game session.
	players []*Player
	// dealingShoeA is the name of the dealing shoe.
	dealingShoeA *BatchOfCards
	// dealingShoeB is the name of the automatic card shuffling device.
	dealingShoeB *BatchOfCards
	// rounds is the number of times an full operation has been executed.
	rounds int
	// payout is the number of chips the player/dealer lost.
	payout int
	// wager is the number of chips the player/dealer won.
	wager int
}

// NewCasinoWar creates a completely new game.
//
// Casino War shall be played on a table having betting areas for eight (8)
// places on one side for the players and the player/dealer. There is a place
// for the Casino dealer on the opposite side of the table. Within each betting
// area there will be a separate circle for the placement of the tie bet.
func NewCasinoWar() *CasinoWar {
	c := new(CasinoWar)

	rand.Seed(time.Now().UnixNano())

	// Cards used to play Casino War shall be played with at least twelve (12)
	// decks of cards in accordance with the following requirements:
	//
	// a. The cards shall be separated into two batches with an equal number
	//    of decks included in each batch.
	// b. The backs of the cards in each batch shall be of the same color and
	//    design, but of a different color than the cards included in the other
	//    batch.
	// c. One batch of the cards will be shuffled and stored in the automated
	//    card shuffling device while the other batch is being dealt or used
	//    to play the game;
	// d. Both batches of cards shall be continuously alternated in and out of
	//    play, with each batch being used for every other dealing shoe; and
	// e. The cards from only one batch shall be placed in the discard rack at
	//    any given time.
	c.dealingShoeA = NewBatchOfCards(6)
	c.dealingShoeB = NewBatchOfCards(6)

	return c
}

// AddPlayer inserts a new player into the game.
//
// A minimum of two and a maximum of eight players including the player/dealer
// position may occupy a seated position in the game. More players may
// participate in the game via backline betting.
func (c *CasinoWar) AddPlayer(name string, chips int) error {
	if c.seats >= maximumPlayers {
		return fmt.Errorf("cannot add more players")
	}

	c.players = append(c.players, &Player{
		Name:  name,
		Chips: chips,
	})

	c.seats++

	return nil
}

// Shuffle rearranges (a deck of cards) by sliding the cards over each other
// quickly, but only the cards that are still in the deck, facing down.
func (c *CasinoWar) Shuffle() {
	c.dealingShoeA.Shuffle()
	c.dealingShoeB.Shuffle()
}

// Play executes one single round of the game, including, but not limited to
// player bets, dealing cards, comparing cards, collecting wagers, evaluating
// war session (if applicable), and terminating the game session.
func (c *CasinoWar) Play() error {
	if c.seats < 1 {
		return io.EOF
	}

	// 1. All wagers at Casino War shall be made by placing gaming chips on
	//    the appropriate betting areas of the table layout, keeping in mind
	//    the table minimum and maximum wagering limits.
	// 2. All wagers shall be placed prior to the dealer announcing "No more
	//    bets." No bets shall be made, increased, or withdrawn after the
	//    dealer has announced, "No more bets."
	// 3. At the beginning of each round of play, each player shall be
	//    required to place a primary wager.
	// 4. Each player at a Casino War table, who has placed a primary wager
	//    as required above, shall also have the option to make the optional
	//    Tie wager.
	// 5. The player/dealer will collect all losing wagers and will pay all
	//    winning wagers to the extent of their wager. Once the player/dealer's
	//    wager is exhausted, all player wagers not covered by the player/dealer
	//    will be returned to the players.
	if err := c.Bet(); err != nil {
		return err
	}

	// Prior to dealing any cards, the dealer shall announce No more bets."
	fmt.Println("No more bets.")

	// Prior to starting the first round of play after the cards have been cut
	// and placed in the dealing shoe, the dealer shall remove the first card
	// from the shoe face down and, without revealing its rank to anyone, place
	// it in the discard rack, which shall be located on the table in front of
	// or to the right of the dealer. Each new dealer who comes to the table
	// shall also discard one burn card before dealing any cards in a round of
	// play.
	if c.rounds == 0 {
		if err := c.dealingShoeA.DiscardCard(); err != nil {
			return err
		}
	}

	fmt.Println()

	// Each card shall be removed from the dealing shoe with the left hand
	// of the dealer and placed face up on the appropriate area of the layout
	// with the right hand of the dealer.
	//
	// Whenever the cutting card is reached in the deal of the cards, the
	// dealer shall continue dealing the cards until that round of play is
	// completed after which the cards shall be reshuffled.
	//
	// No player shall touch any card used in the game of Casino War other
	// than the cutting card.
	dealerCard, hands, err := c.Deal()

	if err != nil {
		return err
	}

	c.VisualizePlayerHands(dealerCard, hands)

	fmt.Println()

	tieHands := c.ComparePlayerHands(dealerCard, hands)

	// There are no tie hands.
	if len(tieHands) == 0 {
		return nil
	}

	fmt.Println()

	playersAtWar := c.DetermineSurrenders(tieHands)

	fmt.Println()

	if err := c.PlayerDealerWar(dealerCard, playersAtWar); err != nil {
		return err
	}

	c.rounds++

	return nil
}

// Bet - Before any cards are dealt, the dealer will call for bets. Players
// decide on the amount they would like to wager, then place their chips in
// their betting space.
func (c *CasinoWar) Bet() error {
	for i, p := range c.players {
		err := p.BetChips()

		if errors.Is(err, errPlayerHasNoChips) || errors.Is(err, errPlayerHasFolded) {
			c.players = append(c.players[0:i], c.players[i+1:]...)
			c.seats--
			continue
		}

		if err != nil {
			return err
		}
	}

	return nil
}

// Deal - The dealer will place two cards face up on the table: a player card
// and a dealer card. The highest card on the table wins. Aces are the highest
// value cards and the suits—whether hearts, diamonds, spades or clubs—don’t
// matter.
//
// The dealer shall, starting with the player farthest to the dealer's left
// and continuing in a clockwise manner, deal the cards as follows:
//
// a. One card face up to each player who has placed on primary wager; and
// b. One card face up to the player/dealer.
func (c *CasinoWar) Deal() (Card, []PlayerHand, error) {
	return c.dealToThesePlayers(c.players)
}

func (c *CasinoWar) dealToThesePlayers(players []*Player) (Card, []PlayerHand, error) {
	if c.dealingShoeA.Quota < 1 {
		return Card{}, nil, errNoMoreCards
	}

	if c.dealingShoeA.Quota < c.seats {
		return Card{}, nil, errNotEnoughCards
	}

	hands := []PlayerHand{}

	for _, p := range players {
		card, err := c.dealingShoeA.Deal()

		if err != nil {
			return Card{}, nil, err
		}

		hands = append(hands, PlayerHand{
			Player: p,
			Card:   card,
		})
	}

	dealerCard, err := c.dealingShoeA.Deal()

	if err != nil {
		return Card{}, nil, err
	}

	return dealerCard, hands, nil
}

func (c *CasinoWar) VisualizePlayerHands(dealerCard Card, hands []PlayerHand) {
	// Print player cards with a short delay for animation purposes.
	for _, hand := range hands {
		fmt.Printf("Dealing card %s for %s\n", hand.Card, hand.Player.Name)
		time.Sleep(time.Millisecond * 300)
	}

	fmt.Printf("Dealing card %s for Dealer\n", dealerCard)
}

func (c *CasinoWar) DetermineSurrenders(hands []PlayerHand) []PlayerHand {
	answer := "" // "y" or "n".
	goToWar := []PlayerHand{}

	// If a player has a tie hand, the player shall be offered one of the
	// following options:
	//
	//   a. The player may surrender one-half of his/her primary wager and
	//      end his/her participation in that round of play. If a player
	//      selects this option, the dealer shall collect one- half of the
	//      player's primary wager and place it in front of the player/dealer
	//      position. The dealer shall return the remaining one-half of the
	//      primary wager to the player. The dealer shall then proceed around
	//      the table in a clockwise direction, repeating the process for
	//      each player with a tie hand who selects this option.
	//   b. The player must place a war wager equal to their primary wager.
	//      The player/dealer will place a wager equal to the player’s primary
	//      wager.
	for _, hand := range hands {
		fmt.Printf("%s, do you want to surrender? (y/n) ", hand.Player.Name)
		fmt.Scanf("%s", &answer)

		if answer == "y" {
			c.wager += hand.Player.Bet / 2
			hand.Player.Surrender()
			continue
		}

		goToWar = append(goToWar, hand)
	}

	return goToWar
}

func (c *CasinoWar) ComparePlayerHands(dealerCard Card, hands []PlayerHand) []PlayerHand {
	tieHands := []PlayerHand{}

	// Compare each player's card with the player/dealer's card.
	//
	// 1. After the dealing procedures above have been completed, the dealer
	//    shall, beginning from the dealer's left and proceeding around the
	//    table in a clockwise direction, compare the rank of each player's
	//    card with that of the player/dealer's card and settle all primary
	//    and tie wagers.
	//
	//   a. If a player's card is lower in rank than the player/dealer's
	//      card, the player shall lose his/her primary wager and, if
	//      applicable, tie wager.
	//   b. If a player's card is higher in rank than the player/dealer's
	//      card, the player shall win his/her primary wager and, if
	//      applicable, lose his/her tie wager.
	//   c. If the player's card and the player/dealer's card are of equal
	//      rank (a tie hand), the player shall be afforded the options
	//      specified in (3) below as to his/her primary wager and, if
	//      applicable, win his or her tie wager.
	for _, hand := range hands {
		switch compareCards(hand.Card, dealerCard) {
		case 1:
			// All winning primary wagers and tie wagers shall be paid by the
			// player/dealer in accordance with the approved payout table as
			// provided below.
			c.payout -= hand.Player.Bet
			hand.Player.WinBet()
			fmt.Printf("Won   %s >gt %s for %s\n", hand.Card, dealerCard, hand.Player.Name)
		case 0:
			tieHands = append(tieHands, hand)
			fmt.Printf("Tie   %s =eq %s for %s\n", hand.Card, dealerCard, hand.Player.Name)
		case -1:
			// All losing primary wagers and tie wagers shall be collected by
			// the dealer and placed in front of the player/dealer.
			c.wager += hand.Player.Bet
			hand.Player.LoseBet()
			fmt.Printf("Lost  %s <lt %s for %s\n", hand.Card, dealerCard, hand.Player.Name)
		}
	}

	return tieHands
}

func compareCards(player Card, dealer Card) int {
	// It is a tie; both cards are the same rank.
	if player.Value == dealer.Value {
		return 0
	}

	// Player wins. Player's card is an Ace and Dealer's card is not an Ace,
	// or Player's card rank is higher than the Dealer's card rank.
	if player.Value == 1 || player.Value > dealer.Value {
		return 1
	}

	// Player lost. Dealer's card is an Ace and Player's card is not an Ace,
	// or Dealer's card rank is higher than the Player's card rank.
	return -1
}

func (c *CasinoWar) PlayerDealerWar(dealerCard Card, playersAtWar []PlayerHand) error {
	warPlayers := []*Player{}
	oldPlayerHands := []PlayerHand{}

	// After settling all primary wagers and tie wagers on the original deal,
	// the dealer shall collect the cards of all players except for the cards
	// of those players with a tie hand who have elected to go to war. The
	// collected cards shall be placed in the discard rack in a manner that
	// permits the reconstruction of each hand of the original deal in case of
	// a question or dispute.
	//
	// If any player elects to make a war wager upon the occurrence of a tie
	// hand, the dealer shall confirm the placement of the war wager and
	// collect the full amount of the player's primary wager. The player's card
	// and the player/dealer's card from the original deal shall remain exposed
	// during the war deal.
	for _, hand := range playersAtWar {
		if err := hand.Player.WarWager(); err != nil {
			fmt.Printf("skip %s; %s", hand.Player.Name, err)
			continue
		}

		warPlayers = append(warPlayers, hand.Player)
		oldPlayerHands = append(oldPlayerHands, hand)
	}

	// No players at war.
	if len(warPlayers) < 1 {
		return nil
	}

	// The war deal shall begin with the dealer discarding three burn cards and
	// then dealing the next card face up to the player farthest to the
	// dealer's left who has placed a war wager. The player's war deal card
	// shall be placed on the table adjacent to the player's card from the
	// original deal. The dealer shall then proceed around the table in a
	// clockwise direction, repeating the process for each player who has
	// placed a war wager and the player/dealer.

	for i := 0; i < 3; i++ {
		if err := c.dealingShoeA.DiscardCard(); err != nil {
			return err
		}
	}

	fmt.Println()
	fmt.Println("Playing war against the player/dealer")
	fmt.Println()

	dealerWarCard, warHands, err := c.dealToThesePlayers(warPlayers)

	if err != nil {
		return err
	}

	c.VisualizePlayerHands(dealerWarCard, warHands)

	fmt.Println()

	// After the dealing procedures above have been completed, the dealer shall,
	// beginning from the dealer's left and proceeding around the table in a
	// clockwise direction, compare the rank of each player's card from the war
	// deal to the player/dealer's card from the war deal and settle all war
	// wagers.
	//
	//   a. If the player's card in the war deal is lower in rank than the
	//      player/dealer's card in the war deal, the player loses.
	//   b. If the player's card in the war deal is higher in rank than the
	//      player/dealer's card in the war deal, the player shall win the
	//      three units wagered.
	//   c. If the player's card and the player/dealer's card in the war
	//      deal are of equal rank, the player shall win the three units
	//      wagered and an additional unit.
	//
	// All losing war wagers shall be collected by the dealer and placed in
	// front of the player/dealer. All winning war wagers shall be paid in
	// accordance with the approved payout odds. After the collection of all
	// losing wagers and the payment of all winning wagers from the war deal,
	// the dealer shall remove all remaining cards from the table and place
	// them in the discard rank in a manner that permits the reconstruction
	// of each hand of the war deal in case of a question or dispute.
	tieHands := c.ComparePlayerHands(dealerCard, warHands)

	// There are no tie hands.
	if len(tieHands) == 0 {
		return nil
	}

	// TODO: tie at war; pay at odds of 2 to 1 of the war wager.
	for _, hand := range tieHands {
		c.payout -= hand.Player.Bet
		hand.Player.WinBet()
	}

	return nil
}
