# Casino War

Casino War is a proprietary casino table game based on the game of [War](https://en.wikipedia.org/wiki/War_(card_game)).

One of the easier table games to learn, Casino War uses a standard 52-card deck and requires no skill to play. The game lets players bet on who will draw a higher card – the player or the dealer.

## References

- https://en.wikipedia.org/wiki/Casino_War#Game_play
- https://oag.ca.gov/sites/all/files/agweb/pdfs/gambling/BGC_war.pdf
- https://en.wikipedia.org/wiki/Casino_token
- https://en.wikipedia.org/wiki/Playing_card
- https://en.wikipedia.org/wiki/Standard_52-card_deck
- https://unicode-table.com/en/sets/suits-of-the-cards/
- https://www.playsmart.ca/novelty-games/casino-war/how-to-play/
- https://www.youtube.com/watch?v=uimPY3I1aw4
- https://www.youtube.com/watch?v=SreZpq4LCdA
